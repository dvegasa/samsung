package com.company.stringcalc;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    // views:
    Button btnCalc;
    EditText input;
    TextView output;
    CheckBox checkBox;
    TextView textBadInput;
    /////////////////////////////
    // input datas:
    String primer;
    /////////////////////////////
    // program datas:
    int num1;
    int num2;
    char znak;
    boolean isNum1 = true;
    char curChar;
    int i = 0;
    boolean stopWalk = false;
    int tempAnswer;
    boolean finishCalc = false;
    boolean getError = false;

    char[] validChars = {'=', '+', '-', '1', '2', '3', '4', '5', '6', '7', '8', '9', '0'};

    private void initViews(){
        btnCalc = (Button) findViewById(R.id.btnCalc);
        input = (EditText) findViewById(R.id.input);
        output = (TextView) findViewById(R.id.output);
        checkBox = (CheckBox) findViewById(R.id.checkBox);
        textBadInput = (TextView) findViewById(R.id.textBadInput);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initViews();
        output.setMovementMethod(new ScrollingMovementMethod());

    }

    public void resetDatas(){
        input.setText("");
        isNum1 = true;
        i = 0;
        stopWalk = false;
        finishCalc = false;
        btnCalc.setEnabled(true);
        primer = "";
    }


    public void btnClick(View view){

        getInput();
        textBadInput.setVisibility(View.INVISIBLE);
        getError = false;
        //if (checkValid()) {
        //    textBadInput.setVisibility(View.INVISIBLE);
            while (!finishCalc) {
                walk();
            }
        //}
        //else{
        //    textBadInput.setVisibility(View.VISIBLE);
        //}

        resetDatas();
        //btnCalc.setEnabled(false);
        //Log.i("fin", "btnClick: finish");
    }

    public void getInput(){
        primer = input.getText().toString().replace(" ", "").replace("=","");
        primer = primer.concat("=");
        output.append(primer + "\n");
        //output.setText(primer);
    }
    /*
    public boolean checkValid(){
        for (int j = 0; j < primer.length(); j++){
            char tempChar = primer.charAt(j);

            for (int k = 0; k < validChars.length; k++){

                if (tempChar == validChars[k]){
                    break;
                }else{
                    return false;
                }
            }
        }
        return true;
    }
    */

    public void walk(){
        i = 0;
        stopWalk = false;
        num1 = 0;
        num2 = 0;
        isNum1 = true;
        while (!stopWalk) {
            curChar = primer.charAt(i);
            checkChar(curChar);

            i++;
        }
        if (finishCalc && !getError){
            output.append("================Ответ: "+ primer.replace("=", "") + "\n" + "\n");
        }else if (checkBox.isChecked() && !getError){
            output.append(primer + "\n");
        }

        if (getError){
            output.append("================Ошибка ввода" + "\n" + "\n");
        }
    }



    public void checkChar(char c){
        try {
            switch (c) {
                case '+': {
                    if (isNum1) {
                        isNum1 = false;
                        znak = '+';
                        break;
                    } else {
                        action(num1, num2, znak);
                        break;
                    }
                }
                case '-': {
                    if (primer.charAt(0)=='-' && i == 0){
                        break;
                    }else if (isNum1) {
                        isNum1 = false;
                        znak = '-';
                        break;
                    } else {
                        action(num1, num2, znak);
                        break;
                    }
                }
                case '=':{
                    if (!isNum1){ // предпоследняя итерация
                        action(num1, num2, znak);
                        break;
                    }else{          // последняя итерация. финиш
                        finishCalc = true;
                        stopWalk = true;
                        break;
                    }
                }
                default:{
                    if (isNum1){
                        num1 = num1 * 10 + Integer.parseInt(String.valueOf(c));
                        break;
                    }else{
                        num2 = num2 * 10 + Integer.parseInt(String.valueOf(c));
                        break;
                    }
                }
            }
        } catch (NumberFormatException e) {
            e.printStackTrace();
            textBadInput.setVisibility(View.VISIBLE);
            getError = true;
        }
    }

    public void action(int a, int b, char z){
        if (primer.charAt(0) == '-'){
            a *= -1;
        }

        switch(z){
            case '+':{
                tempAnswer = a + b;
                break;
            }
            case '-':{
                tempAnswer = a - b;
                break;
            }
            default:{

            }
        }

        primer = String.valueOf(tempAnswer) + primer.substring(i);
        stopWalk = true;
    }
}
